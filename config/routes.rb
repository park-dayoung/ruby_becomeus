Rails.application.routes.draw do

  get 'orders/add_product'

  get 'admin/permission'
  get 'admin/:id/edit' => 'admin#edit', :as => "admin_edit"
  patch 'admin/:id' => 'admin#update'
  put 'admin/:id' => 'admin#update'

  get 'orders/search_test'
  get 'orders/add_customer'

  resources :orders do
    collection do
      delete 'destroy_multiple'
      get 'search_action'
      get 'search_result'
      get 'sku_select'
    end
  end
  resources :delivery_agencies do
    collection do
      delete 'destroy_multiple'
    end
  end
  resources :business_groups do
    collection do
      delete 'destroy_multiple'
    end
  end
  resources :suppliers do
    collection do
      delete 'destroy_multiple'
    end
  end

  devise_for :users, controllers: {
      sessions: 'users/sessions'
  }

  resources :products do
    collection do
      delete 'destroy_multiple'
    end
  end

  resources :customers do
      collection do
        delete 'destroy_multiple'
      end
    end

  devise_scope :user do
    root to: "devise/sessions#new"
  end
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
