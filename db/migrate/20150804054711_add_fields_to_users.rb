class AddFieldsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :name, :string
    add_column :users, :isok, :boolean
    add_column :users, :admin, :boolean
  end
end
