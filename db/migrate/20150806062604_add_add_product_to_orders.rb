class AddAddProductToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :size, :string
    add_column :orders, :color, :string
    add_column :orders, :quantity, :integer
    add_column :orders, :total, :integer
  end
end
