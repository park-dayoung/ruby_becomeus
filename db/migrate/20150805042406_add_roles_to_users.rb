class AddRolesToUsers < ActiveRecord::Migration
  def change
    add_column :users, :isok, :boolean, :default => false
    add_column :users, :staff, :boolean, :default => false
  end
end
