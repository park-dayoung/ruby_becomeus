class CreateSuppliers < ActiveRecord::Migration
  def change
    create_table :suppliers do |t|
      t.string :company_name
      t.string :staff_name
      t.string :suppliers_code
      t.string :gender
      t.string :contact_email
      t.string :contact_number
      t.text :address

      t.timestamps null: false
    end
  end
end
