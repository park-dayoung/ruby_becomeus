class ChangeColumnMarketplaceFromProducts < ActiveRecord::Migration
  def change
  	change_column :products, :marketplace, :string, array: true
  end
end
