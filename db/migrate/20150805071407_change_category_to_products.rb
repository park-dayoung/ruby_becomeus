class ChangeCategoryToProducts < ActiveRecord::Migration
  def change
  	rename_column :products, :category_string, :category
  end
end
