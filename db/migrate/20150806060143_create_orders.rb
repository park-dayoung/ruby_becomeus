class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :sales_owner
      t.string :marketplace
      t.string :customer
      t.string :order_date
      t.string :product
      t.string :notes
      t.string :delivery_date
      t.string :delivery_agency
      t.string :track_number

      t.timestamps null: false
    end
  end
end
