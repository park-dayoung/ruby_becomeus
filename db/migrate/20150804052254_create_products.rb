class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :business_group
      t.string :category_string
      t.string :supplier
      t.string :brand
      t.string :product_code
      t.decimal :price_cny
      t.decimal :price_krw
      t.decimal :stock
      t.string :variation
      t.string :color
      t.string :size
      t.decimal :weight
      t.text :dimension
      t.text :material_m
      t.text :material_e
      t.string :product_name
      t.text :description
      t.text :keyword
      t.string :marketplace
      t.string :status

      t.timestamps null: false
    end
  end
end
