class RemoveFieldsFromUsers < ActiveRecord::Migration
  def change
    remove_column :users, :admin, :boolean
    remove_column :users, :isok, :boolean
    remove_column :users, :staff, :boolean
  end
end
