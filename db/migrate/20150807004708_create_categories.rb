class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string :classification
      t.string :node_id
      t.string :node_path
      t.string :cat1
      t.string :cat2
      t.string :cat3
      t.string :cat4
      t.string :cat5
      t.string :cat6
      t.string :cat7
      t.string :inventory
      t.string :class_field1
      t.string :valid_val1
      t.string :class_field2
      t.string :valid_val2
    end
  end
end
