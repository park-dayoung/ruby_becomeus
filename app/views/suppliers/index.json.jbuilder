json.array!(@suppliers) do |supplier|
  json.extract! supplier, :id, :company_name, :staff_name, :suppliers_code, :gender, :contact_email, :contact_number, :address
  json.url supplier_url(supplier, format: :json)
end
