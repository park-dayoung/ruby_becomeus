json.array!(@business_groups) do |business_group|
  json.extract! business_group, :id, :name
  json.url business_group_url(business_group, format: :json)
end
