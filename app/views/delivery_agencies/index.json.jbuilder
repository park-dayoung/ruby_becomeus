json.array!(@delivery_agencies) do |delivery_agency|
  json.extract! delivery_agency, :id, :name, :email, :phone, :address
  json.url delivery_agency_url(delivery_agency, format: :json)
end
