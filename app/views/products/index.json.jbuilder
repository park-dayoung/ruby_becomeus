json.array!(@products) do |product|
  json.extract! product, :id, :business_group, :category_string, :supplier, :brand, :product_code, :price_cny, :price_krw, :stock, :variation, :color, :size, :weight, :dimension, :material_m, :material_e, :product_name, :description, :keyword, :marketplace, :status
  json.url product_url(product, format: :json)
end
