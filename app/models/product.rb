class Product < ActiveRecord::Base
	has_attached_file :image, :styles => { :medium => "200x", :thumb => "100x" },  :default_url => ("default.jpg")
	validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/

	serialize :marketplace, Array

	belongs_to :user

	belongs_to :order, class_name: "Order"

	has_many :marketplaces, class_name: "Marketplace", foreign_key: "id"

	def self.search(search)
		where("product_code LIKE ?", "%#{search}%")
	end
end
