class Order < ActiveRecord::Base
	has_one :customer, class_name: "Customer", foreign_key: "id", :dependent => :destroy
	accepts_nested_attributes_for :customer
	has_many :products, class_name: "Product", foreign_key: "product_code"
end
