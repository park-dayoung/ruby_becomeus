class OrdersController < ApplicationController
  require 'will_paginate/array'

  before_action :set_order, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /orders
  # GET /orders.json
  def index
    if current_user && current_user.staff?
      @orders = Order.where(user: current_user).order("created_at DESC")
    else
      @orders = Order.all.order("created_at DESC")
    end
    # @orders = Order.all
    @per_page = params[:per_page] || Order.per_page || 2
    @orders = @orders.paginate( :per_page => @per_page, :page => params[:page])
  end

  # GET /orders/1
  # GET /orders/1.json
  def show
  end

  def search_test
    if params[:search]
      @products = Product.search(params[:search])
    else
      @products = Product.all
    end
  end

  def search_action

    if params[:params]
      @products = Product.search(params[:params])
    else
      @products = Product.all
    end

    respond_to do |format|
      format.js
    end
  end

  def search_result
    @product = Product.find(params[:id])
    @order.price = @product.price_cny
  end

  def sku_select
    @order = Order.new
    if params[:params]
      @product = Product.find(params[:params])
      @order.price = @product.price_cny
    end
  end

  # GET /orders/new
  def new
    #@customer = Customer.new
    @order = Order.new
    @customer = @order.build_customer
    #@products = Product.all

    #@order.customer = @customer.name

    if params[:search]
      @products = Product.search(params[:search])
    else
      @products = Product.all
    end

    if params[:id]
      @product = Product.find(params[:id])
      @order.price = @product.price_cny
      @order.total = @order.price * params[:quan]
    end
  end

  # GET /orders/1/edit
  def edit
    #@customer = Customer.all
  end

  # POST /orders
  # POST /orders.json
  def create
    #@customer = Customer.new(customer_params)
    #@customer.save
    @order = Order.new(order_params)
    @order.customer_name = params[:order][:customer_attributes][:name]
    @order.order_date = @order.created_at
    #@product.category = params[:product][:category]

    #@customer = Customer.new(params[:customer_attributes])
    #@customer = @order.customer_attributes(params[:order])
    #@customer = Customer.find(params[:customer])
    #@customer = @order.create_customer(params[:customer])
    #@customer = Customer.find(params[:id])
    #@customer = Customer.new({:name, :email, :number, :address})

    #@customer.save
    #@customer = Customer.new
    #@customer = @order.create_customer(params[:id])
    #@order.customer_id = @customer.id
    #@order.customer = @customer.name
    #@order.save
    #@customer.name = @order.customer

    respond_to do |format|
      if @order.save
        format.html { redirect_to @order, notice: 'Order was successfully created.' }
        format.json { render :show, status: :created, location: @order }
      else
        format.html { render :new }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /orders/1
  # PATCH/PUT /orders/1.json
  def update
    respond_to do |format|
      if @order.update(order_params)
        format.html { redirect_to @order, notice: 'Order was successfully updated.' }
        format.json { render :show, status: :ok, location: @order }
      else
        format.html { render :edit }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /orders/1
  # DELETE /orders/1.json
  def destroy
    @order.destroy
    respond_to do |format|
      format.html { redirect_to orders_url, notice: 'Order was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def destroy_multiple
    Order.destroy(params[:orders])
    respond_to do |format|
      format.html {redirect_to orders_url, notice: 'Orders were successfully destroyed.'}
      format.json {head :no_content}
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_order
      @order = Order.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def order_params
      params.require(:order).permit(:sales_owner, :marketplace, :customer_name, :order_date, :product, :notes, :delivery_date, :delivery_agency, :track_number, :size, :color, :price, :quantity, :total, :customer_id, customer_attributes: [:name, :email, :number, :address])
    end
    def customer_params
      params.require(:customer).permit(:name, :email, :number, :address)
    end
end
