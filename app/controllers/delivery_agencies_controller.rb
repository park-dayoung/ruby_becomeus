class DeliveryAgenciesController < ApplicationController
  require 'will_paginate/array'

  before_action :set_delivery_agency, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /delivery_agencies
  # GET /delivery_agencies.json
  def index
    @delivery_agencies = DeliveryAgency.all
    @per_page = params[:per_page] || DeliveryAgency.per_page || 2
    @delivery_agencies = @delivery_agencies.paginate( :per_page => @per_page, :page => params[:page])
  end

  # GET /delivery_agencies/1
  # GET /delivery_agencies/1.json
  def show
  end

  # GET /delivery_agencies/new
  def new
    @delivery_agency = DeliveryAgency.new
  end

  # GET /delivery_agencies/1/edit
  def edit
  end

  # POST /delivery_agencies
  # POST /delivery_agencies.json
  def create
    @delivery_agency = DeliveryAgency.new(delivery_agency_params)

    respond_to do |format|
      if @delivery_agency.save
        format.html { redirect_to @delivery_agency, notice: 'Delivery agency was successfully created.' }
        format.json { render :show, status: :created, location: @delivery_agency }
      else
        format.html { render :new }
        format.json { render json: @delivery_agency.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /delivery_agencies/1
  # PATCH/PUT /delivery_agencies/1.json
  def update
    respond_to do |format|
      if @delivery_agency.update(delivery_agency_params)
        format.html { redirect_to @delivery_agency, notice: 'Delivery agency was successfully updated.' }
        format.json { render :show, status: :ok, location: @delivery_agency }
      else
        format.html { render :edit }
        format.json { render json: @delivery_agency.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /delivery_agencies/1
  # DELETE /delivery_agencies/1.json
  def destroy
    @delivery_agency.destroy
    respond_to do |format|
      format.html { redirect_to delivery_agencies_url, notice: 'Delivery agency was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def destroy_multiple
    DeliveryAgency.destroy(params[:delivery_agencies])
    respond_to do |format|
      format.html {redirect_to delivery_agencies_url, notice: 'Delivery Agencies were successfully destroyed.'}
      format.json {head :no_content}
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_delivery_agency
      @delivery_agency = DeliveryAgency.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def delivery_agency_params
      params.require(:delivery_agency).permit(:name, :email, :phone, :address)
    end
end
