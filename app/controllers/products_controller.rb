class ProductsController < ApplicationController
  before_action :set_product, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  require 'will_paginate/array'

  # GET /products
  # GET /products.json
  def index
    if current_user && current_user.staff?
      @products = Product.where(user: current_user).order("created_at DESC")
    else
      @products = Product.all.order("created_at DESC")
    end
    #@products = Product.all
    @per_page = params[:per_page] || Product.per_page || 2
    @products = @products.paginate( :per_page => @per_page, :page => params[:page])
  end

  # GET /products/1
  # GET /products/1.json
  def show
  end

  # GET /products/new
  def new
    @product = Product.new
    @marketplaces = Marketplace.all
  end

  # GET /products/1/edit
  def edit
    @marketplaces = Marketplace.all

    #params['div_category'] = @product.category
  end

  # POST /products
  # POST /products.json
  def create
    @product = Product.new(product_params)
    @product.user_id = current_user.id

    @product.category = params[:product][:category]

    respond_to do |format|
      if @product.save
        format.html { redirect_to @product, notice: 'Product was successfully created.' }
        format.json { render :show, status: :created, location: @product }
      else
        format.html { render :new }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /products/1
  # PATCH/PUT /products/1.json
  def update
    @product.modi_user_name = current_user.name
    respond_to do |format|
      if @product.update(product_params)
        format.html { redirect_to @product, notice: 'Product was successfully updated.' }
        format.json { render :show, status: :ok, location: @product }
      else
        format.html { render :edit }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /products/1
  # DELETE /products/1.json
  def destroy
    @product.destroy
    respond_to do |format|
      format.html { redirect_to products_url, notice: 'Product was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def destroy_multiple
    Product.destroy(params[:products])
    respond_to do |format|
      format.html {redirect_to products_url, notice: 'Products were successfully destroyed.'}
      format.json {head :no_content}
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Product.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def product_params
      params.require(:product).permit(:business_group, :category, :supplier, :brand, :product_code, :price_cny, :price_krw, :stock, :variation, :color, :size, :weight, :dimension, :material_m, :material_e, :product_name, :description, :keyword, {:marketplace => []}, :status, :image)
    end
end
