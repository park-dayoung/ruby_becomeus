class BusinessGroupsController < ApplicationController
  require 'will_paginate/array'

  before_action :set_business_group, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /business_groups
  # GET /business_groups.json
  def index
    @business_groups = BusinessGroup.all
    @per_page = params[:per_page] || BusinessGroup.per_page || 2
    @business_groups = @business_groups.paginate( :per_page => @per_page, :page => params[:page])
  end

  # GET /business_groups/1
  # GET /business_groups/1.json
  def show
  end

  # GET /business_groups/new
  def new
    @business_group = BusinessGroup.new
  end

  # GET /business_groups/1/edit
  def edit
  end

  # POST /business_groups
  # POST /business_groups.json
  def create
    @business_group = BusinessGroup.new(business_group_params)

    respond_to do |format|
      if @business_group.save
        format.html { redirect_to @business_group, notice: 'Business group was successfully created.' }
        format.json { render :show, status: :created, location: @business_group }
      else
        format.html { render :new }
        format.json { render json: @business_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /business_groups/1
  # PATCH/PUT /business_groups/1.json
  def update
    respond_to do |format|
      if @business_group.update(business_group_params)
        format.html { redirect_to @business_group, notice: 'Business group was successfully updated.' }
        format.json { render :show, status: :ok, location: @business_group }
      else
        format.html { render :edit }
        format.json { render json: @business_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /business_groups/1
  # DELETE /business_groups/1.json
  def destroy
    @business_group.destroy
    respond_to do |format|
      format.html { redirect_to business_groups_url, notice: 'Business group was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def destroy_multiple
    BusinessGroup.destroy(params[:business_groups])
    respond_to do |format|
      format.html {redirect_to business_groups_url, notice: 'Business groups were successfully destroyed.'}
      format.json {head :no_content}
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_business_group
      @business_group = BusinessGroup.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def business_group_params
      params.require(:business_group).permit(:name)
    end
end
