class AdminController < ApplicationController

  before_action :set_user, only: [:edit, :update]
  before_action :authenticate_user!

  def permission
  	@users = User.all.order("created_at DESC")
  end

  def show
  end

  def edit
  end

  def update
    respond_to do |format|
      if @user.update_attributes(user_params)
        format.html { redirect_to admin_permission_path, notice: 'User was successfully updated.' }
        format.json { render :permission, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to admin_permission_path, notice: 'Business group was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:name, :email, :isok, :staff)
    end
end
