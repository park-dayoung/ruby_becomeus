// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require bootstrap-sprockets
//= require bootstrap-datepicker
//= require turbolinks
//= require_tree .

$(document).ready(function() {
	ready();
});

$(document).on('page:load', function() {
	ready();
});

function _variation() {
	if ( $('select.variation').val() == 'size' ) {
		$('div.size').show();
		$('div.color').hide();
	} else if ( $('select.variation').val() == 'color' ) {
		$('div.size').hide();
		$('div.color').show();
	} else if ( $('select.variation').val() == 'size_and_color' ) {
		$('div.size').show();
		$('div.color').show();
	} else {
		$('div.size').hide();
		$('div.color').hide();
	}
};

function more() {
	$('div.more_hide').show();
	$('input.btn_more').hide();
	$('input.btn_hide').show();
};

function hide() {
	$('div.more_hide').hide();
	$('input.btn_more').show();
	$('input.btn_hide').hide();
};

function ready() {
	$('.datepicker').datepicker();

	_variation();

	$('#cat2, #cat3, #cat4, #cat5, #cat6, #cat7').css("visibility", "hidden");
	var html = "Clothing, Shoes & Jewelry";
	var html1, html2, html3, html4, html5, html6, html7

	$('div#div_category').html(html);
	$('input#product_category').val(html);

	$('#cat1').change(function() {
		$('#cat2, #cat3, #cat4, #cat5, #cat6, #cat7').css("visibility", "hidden");
		html1 = html + " > " + $('#cat1').val();
		
		$('div#div_category').html(html1);
		$('input#product_category').val(html1);
		$('#cat2').css("visibility", "visible");
	});

	$('#cat2').change(function() {
		$('#cat3, #cat4, #cat5, #cat6, #cat7').css("visibility", "hidden");
		html2 = html1 + " > " + $('#cat2').val();
		
		$('div#div_category').html(html2);
		$('input#product_category').val(html2);
		$('#cat3').css("visibility", "visible");
	});
	$('#cat3').change(function() {
		$('#cat4, #cat5, #cat6, #cat7').css("visibility", "hidden");
		html3 = html2 + " > " + $('#cat3').val();
		
		$('div#div_category').html(html3);
		$('input#product_category').val(html3);
		$('#cat4').css("visibility", "visible");
	});
	$('#cat4').change(function() {
		$('#cat5, #cat6, #cat7').css("visibility", "hidden");
		html4 = html3 + " > " + $('#cat4').val();
		
		$('div#div_category').html(html4);
		$('input#product_category').val(html4);
		$('#cat5').css("visibility", "visible");
	});
	$('#cat5').change(function() {
		$('#cat6, #cat7').css("visibility", "hidden");
		html5 = html4 + " > " + $('#cat5').val();
		
		$('div#div_category').html(html5);
		$('input#product_category').val(html5);
		$('#cat6').css("visibility", "visible");
	});
	$('#cat6').change(function() {
		$('#cat7').css("visibility", "hidden");
		html6 = html5 + " > " + $('#cat6').val();
		
		$('div#div_category').html(html6);
		$('input#product_category').val(html6);
		$('#cat7').css("visibility", "visible");
	});
	$('#cat7').change(function() {
		//$('#cat7').css("visibility", "hidden");
		html7 = html6 + " > " + $('#cat7').val();
		
		$('div#div_category').html(html7);
		$('input#product_category').val(html7);
		//$('#cat7').css("visibility", "visible");
	});
}