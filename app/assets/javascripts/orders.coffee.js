$(document).ready(function() {
	total();
});

$(document).on('page:load', function() {
	total();
});


function sku_select(id) {
	/*$('#order_product').change(function() {
		$('#order_price').val(100);
		var pri = parseInt($('#order_price').val());
		var quan = parseInt($('#order_quantity').val());
		var total = pri*quan;
		$('#order_total').val(total);
	});*/

	//var params = $(this).val();
	var quan = parseInt($('#order_quantity').val());
	$.ajax({
		url: 'new',
		data: {id: id, quan: quan}
	});
}

function total() {
	$('#order_price, #order_quantity').change(function() {
		var pri = parseInt($('#order_price').val());
		var quan = parseInt($('#order_quantity').val());
		var total = pri*quan;
		$('#order_total').val(total);
	});
}

function refreshPartial() {
	var params = $('#search_field').val();
	$.ajax({
		url: 'search_action',
		data: {params: params}
	});
}