require 'test_helper'

class AdminControllerTest < ActionController::TestCase
  test "should get permission" do
    get :permission
    assert_response :success
  end

  test "should get edit" do
    get :edit
    assert_response :success
  end

end
