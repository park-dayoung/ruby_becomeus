require 'test_helper'

class ProductsControllerTest < ActionController::TestCase
  setup do
    @product = products(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:products)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create product" do
    assert_difference('Product.count') do
      post :create, product: { brand: @product.brand, business_group: @product.business_group, category_string: @product.category_string, color: @product.color, description: @product.description, dimension: @product.dimension, keyword: @product.keyword, marketplace: @product.marketplace, material_e: @product.material_e, material_m: @product.material_m, price_cny: @product.price_cny, price_krw: @product.price_krw, product_code: @product.product_code, product_name: @product.product_name, size: @product.size, status: @product.status, stock: @product.stock, supplier: @product.supplier, variation: @product.variation, weight: @product.weight }
    end

    assert_redirected_to product_path(assigns(:product))
  end

  test "should show product" do
    get :show, id: @product
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @product
    assert_response :success
  end

  test "should update product" do
    patch :update, id: @product, product: { brand: @product.brand, business_group: @product.business_group, category_string: @product.category_string, color: @product.color, description: @product.description, dimension: @product.dimension, keyword: @product.keyword, marketplace: @product.marketplace, material_e: @product.material_e, material_m: @product.material_m, price_cny: @product.price_cny, price_krw: @product.price_krw, product_code: @product.product_code, product_name: @product.product_name, size: @product.size, status: @product.status, stock: @product.stock, supplier: @product.supplier, variation: @product.variation, weight: @product.weight }
    assert_redirected_to product_path(assigns(:product))
  end

  test "should destroy product" do
    assert_difference('Product.count', -1) do
      delete :destroy, id: @product
    end

    assert_redirected_to products_path
  end
end
