require 'test_helper'

class DeliveryAgenciesControllerTest < ActionController::TestCase
  setup do
    @delivery_agency = delivery_agencies(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:delivery_agencies)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create delivery_agency" do
    assert_difference('DeliveryAgency.count') do
      post :create, delivery_agency: { address: @delivery_agency.address, email: @delivery_agency.email, name: @delivery_agency.name, phone: @delivery_agency.phone }
    end

    assert_redirected_to delivery_agency_path(assigns(:delivery_agency))
  end

  test "should show delivery_agency" do
    get :show, id: @delivery_agency
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @delivery_agency
    assert_response :success
  end

  test "should update delivery_agency" do
    patch :update, id: @delivery_agency, delivery_agency: { address: @delivery_agency.address, email: @delivery_agency.email, name: @delivery_agency.name, phone: @delivery_agency.phone }
    assert_redirected_to delivery_agency_path(assigns(:delivery_agency))
  end

  test "should destroy delivery_agency" do
    assert_difference('DeliveryAgency.count', -1) do
      delete :destroy, id: @delivery_agency
    end

    assert_redirected_to delivery_agencies_path
  end
end
