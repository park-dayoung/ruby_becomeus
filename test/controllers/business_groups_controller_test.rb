require 'test_helper'

class BusinessGroupsControllerTest < ActionController::TestCase
  setup do
    @business_group = business_groups(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:business_groups)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create business_group" do
    assert_difference('BusinessGroup.count') do
      post :create, business_group: { name: @business_group.name }
    end

    assert_redirected_to business_group_path(assigns(:business_group))
  end

  test "should show business_group" do
    get :show, id: @business_group
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @business_group
    assert_response :success
  end

  test "should update business_group" do
    patch :update, id: @business_group, business_group: { name: @business_group.name }
    assert_redirected_to business_group_path(assigns(:business_group))
  end

  test "should destroy business_group" do
    assert_difference('BusinessGroup.count', -1) do
      delete :destroy, id: @business_group
    end

    assert_redirected_to business_groups_path
  end
end
